An extension to LuaJIT

# Libraries
* [Lua-Marshal](https://github.com/richardhundt/lua-marshal)
* [LuaFileSystem](https://github.com/keplerproject/luafilesystem)

# Extended functionality
## string
* trim()
* trim_l()
* trim_r()
* trim_w()
# table
* keys()
* values()
* size()
* random()

# How-to
* require("lua_extensions")
* local marshal = require("marshal")
* local lfs = require("lfs")